﻿using NUnit.Framework;
using System.Collections.Generic;
using GreedyMatching.Helpers;

namespace GreedyMatchingTests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void WhenSentFragmentsOfTests_ItReassemblesCorrectly()
        {
            //arrange
//            var originalText = "all is well that ends well";
//            var strings = new List<string> {"t ends well", "hat end", "ell that en", "all is well"};

            var originalText = "A little computer science theory aside";
            var strings = new List<string> {"ittle computer sci", "A litt", "ce theory aside", "uter science theo"};
            
            //act
            var result = GreedyMatchingHelper.StartGreedyMatch(strings);
            
            Assert.AreEqual(originalText, result);
        }
    }
}