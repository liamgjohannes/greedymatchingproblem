using System.Collections.Generic;
using System.Linq;
using GreedyMatching.Models;

namespace GreedyMatching.Helpers
{
    public static class GreedyMatchingHelper
    {
        public static string StartGreedyMatch(List<string> strings)
        {
            var resultSet = new List<GreedyMatchingResultsModel>();
            
            foreach (var @string in strings)
            {
                var searchModel = GetGreedyMatchingSearchModel(strings, @string);

                foreach (var comparerString in searchModel.ComparerStrings)
                {
                    const int indexOfComparerString = 0;
                    var indexesOfMasterString = GetAllIndexesOfString(searchModel.MasterString, comparerString[indexOfComparerString]);

                    foreach (var indexOfMasterString in indexesOfMasterString)
                    {
                        if (indexOfMasterString < 0)
                        {
                            resultSet.Add(new GreedyMatchingResultsModel
                            {
                                MasterString = searchModel.MasterString,
                                ComparerString = comparerString,
                                MatchedCharacters = 0
                            });
                            continue;
                        }

                        var resultPrefix = GetResultPrefix(searchModel.MasterString, indexOfMasterString);
                        var numberOfMatchedChars = GetNumberOfConcurrentCharacterMatches(comparerString, indexOfComparerString, searchModel, indexOfMasterString);
                        var resultSuffix = GetResultSuffix(searchModel.MasterString, indexOfMasterString, comparerString);

                        resultSet.Add(new GreedyMatchingResultsModel
                        {
                            MasterString = searchModel.MasterString,
                            ComparerString = comparerString,
                            MatchedCharacters = numberOfMatchedChars,
                            MergedString = $"{resultPrefix}{resultSuffix}"
                        });
                    }
                }
            }

            strings = CleanupSearchStringList(strings, resultSet);

            if (strings.Count > 1)
            {
                StartGreedyMatch(strings);
            }

            return strings.First();
        }

        private static List<string> CleanupSearchStringList(List<string> strings, IEnumerable<GreedyMatchingResultsModel> resultSet)
        {
            var greediestResult = resultSet.Aggregate((x, y) => x.MatchedCharacters > y.MatchedCharacters ? x : y);

            strings.Remove(greediestResult.MasterString);
            strings.Remove(greediestResult.ComparerString);
            strings.Add(greediestResult.MergedString);
            
            return strings;
        }

        private static GreedyMatchingSearchModel GetGreedyMatchingSearchModel(IEnumerable<string> searchSet, string masterString)
        {
            return new GreedyMatchingSearchModel
            {
                MasterString = masterString,
                ComparerStrings = searchSet.Where(x => x != masterString).ToList()
            };
        }

        private static IEnumerable<int> GetAllIndexesOfString(string stringToSearch, char searchPattern)
        {
            return stringToSearch.Select((s, p) => s == searchPattern ? p : -1).Where(i => i != -1).ToList();
        }

        private static int GetNumberOfConcurrentCharacterMatches(string comparerString, int indexOfComparerString, GreedyMatchingSearchModel searchModel, int indexOfMasterString)
        {
            var matchedChars = 0;
            while (comparerString[indexOfComparerString] == searchModel.MasterString[indexOfMasterString] 
                   && indexOfComparerString < comparerString.Length - 1 
                   && indexOfMasterString < searchModel.MasterString.Length - 1)
            {
                matchedChars++;
                indexOfComparerString++;
                indexOfMasterString++;
            }

            if (indexOfComparerString < comparerString.Length - 1 && indexOfMasterString < searchModel.MasterString.Length - 1)
            {
                return 0;
            }

            return matchedChars;
        }

        private static string GetResultPrefix(string searchModelMasterString, int indexOfMasterString)
        {
            return searchModelMasterString.Substring(0 , indexOfMasterString);
        }

        private static string GetResultSuffix(string searchModelMasterString, int indexOfMasterString, string comparerString)
        {
            var masterStringLeftovers = searchModelMasterString.Except(GetResultPrefix(searchModelMasterString, indexOfMasterString)).ToList();
            
            return comparerString.Length >= masterStringLeftovers.Count ? comparerString : $"{comparerString}{masterStringLeftovers.Except(comparerString)}";
        }
    }
}