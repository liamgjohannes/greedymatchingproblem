using System.Collections.Generic;

namespace GreedyMatching.Models
{
    public class GreedyMatchingSearchModel
    {
        public string MasterString { get; set; }
        public List<string> ComparerStrings { get; set; }
    }
}