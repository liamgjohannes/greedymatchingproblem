﻿using System;
using System.Collections.Generic;
using GreedyMatching.Helpers;

namespace GreedyMatching
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var strings = new List<string> {"t ends well", "hat end", "ell that en", "all is well"};
            
            var result = GreedyMatchingHelper.StartGreedyMatch(strings);

            Console.WriteLine(result);
        }
    }
}