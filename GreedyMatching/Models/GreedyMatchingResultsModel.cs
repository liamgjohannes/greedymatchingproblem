namespace GreedyMatching.Models
{
    public class GreedyMatchingResultsModel
    {
        public string MasterString { get; set; }
        public string ComparerString { get; set; }
        public int MatchedCharacters { get; set; }
        public string MergedString { get; set; }
    }
}